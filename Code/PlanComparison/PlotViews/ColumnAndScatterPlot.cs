﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;

namespace PlanComparison.PlotViews
{
    class ColumnAndScatterPlot : IPlotView
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            var linearXAxis = new OxyPlot.Axes.LinearAxis
            {
                Key = "Linear",
                IsAxisVisible = false,
                MajorGridlineStyle = LineStyle.None,
                Position = AxisPosition.Bottom,
                MajorStep = 1,
                Minimum = xMin,
                Maximum = xMax
            };

            var catXAxis = new OxyPlot.Axes.CategoryAxis
            {
                Key = "Plan ID",
                MajorGridlineStyle = LineStyle.None,
                Position = AxisPosition.Bottom,
                MajorStep = 1,
                Minimum = xMin,
                Maximum = xMax,
                Title = xLabel
            };

            catXAxis.Labels.AddRange(xTickLabels);

            var yAxisLeft = new OxyPlot.Axes.LinearAxis
            {
                Key = "Left",
                Position = AxisPosition.Left,
                MajorGridlineStyle = LineStyle.None,
                AxislineColor = OxyColors.RoyalBlue,
                TitleColor = OxyColors.RoyalBlue,
                TextColor = OxyColors.RoyalBlue,
                TicklineColor = OxyColors.RoyalBlue,
                MinorTicklineColor = OxyColors.RoyalBlue,
                Title = yLabel[0]
            };

            var yAxisRight = new OxyPlot.Axes.LinearAxis
            {
                Key = "Right",
                Position = AxisPosition.Right,
                MajorGridlineStyle = LineStyle.None,
                Title = yLabel[1],
                Minimum = 0,
                MaximumPadding = 0.1
            };

            plotModel.Axes.Add(linearXAxis);
            plotModel.Axes.Add(catXAxis);
            plotModel.Axes.Add(yAxisLeft);
            plotModel.Axes.Add(yAxisRight);
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, LegendPlacement placement, LegendPosition position, LegendOrientation orientation)
        {
            plotModel.IsLegendVisible = false;
        }

        public void SetTitle(PlotModel plotModel, string title)
        {
            plotModel.Title = title;
        }
    }
}
