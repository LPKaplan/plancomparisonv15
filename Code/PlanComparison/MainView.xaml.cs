﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.Win32;
using OxyPlot.Wpf;
using OxyPlot;
using OxyPlot.Pdf;
using VMS.TPS.Common.Model.Types;
using VMS.TPS.Common.Model.API;

namespace PlanComparison
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        // Fields
        private readonly MainViewModel _vm;
        private readonly string currentPath;

        // Constructor
        public MainView(MainViewModel viewModel)
        {
            // Get current path (for save files)
            string path = new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            string dllName = Assembly.GetExecutingAssembly().GetName().Name + ".dll";
            currentPath = path.Replace(dllName, "");

            _vm = viewModel;

            InitializeComponent();
            DataContext = viewModel;

            // Load variables from previous session
            LoadSavedVariables();
        }

        // Method to load previously saved variables
        private void LoadSavedVariables()
        {
            // load saved input values to variables
            _vm.NumericInput_1 = Properties.Settings.Default.numericInput_1;
            _vm.NumericInput_2 = Properties.Settings.Default.numericInput_2;
            _vm.NumericInput_3 = Properties.Settings.Default.numericInput_3;
            _vm.NumericInput_4 = Properties.Settings.Default.numericInput_4;
            _vm.NumericInput_5 = Properties.Settings.Default.numericInput_5;

            _vm.ChosenPlotType = Properties.Settings.Default.chosenPlot;
            _vm.ChosenStructureId = Properties.Settings.Default.chosenStructure;
            _vm.ChosenSecondStructureId = Properties.Settings.Default.chosenSecondStructure;
            _vm.AbsRelDose = Properties.Settings.Default.AbsRelDose;
            _vm.AbsRelVol = Properties.Settings.Default.AbsRelVol;
            _vm.Direction = Properties.Settings.Default.chosenDirection;

            // show saved input values in GUI
            NumericInputBox.Text = Properties.Settings.Default.numericInput_1.ToString();
            TwoNumericInputsBox1.Text = Properties.Settings.Default.numericInput_1.ToString();
            TwoNumericInputsBox2.Text = Properties.Settings.Default.numericInput_2.ToString();
            ThreeNumericInputsBox1.Text = Properties.Settings.Default.numericInput_1.ToString();
            ThreeNumericInputsBox2.Text = Properties.Settings.Default.numericInput_2.ToString();
            ThreeNumericInputsBox3.Text = Properties.Settings.Default.numericInput_3.ToString();
            FiveNumericInputsBox1.Text = Properties.Settings.Default.numericInput_1.ToString();
            FiveNumericInputsBox2.Text = Properties.Settings.Default.numericInput_2.ToString();
            FiveNumericInputsBox3.Text = Properties.Settings.Default.numericInput_3.ToString();
            FiveNumericInputsBox4.Text = Properties.Settings.Default.numericInput_4.ToString();
            FiveNumericInputsBox5.Text = Properties.Settings.Default.numericInput_5.ToString();
        }

        #region Plot selection logic
        private void PlotsComboBoxLoaded(object sender, RoutedEventArgs e)
        {
            // List of possible plot to choose from. Expand as they are coded.
            List<string> plots = new List<string>();
            plots.Add("Empty");
            plots.Add("Scatter: Conformity, Homogeneity, Gradient - CN and simplified GI");
            plots.Add("Scatter: Conformity, Homogeneity, Gradient - CDI and GCI");
            plots.Add("Scatter: Target cold volume size and distance from edge");
            plots.Add("Scatter: Structure hot volume size and distance from structure edge");
            plots.Add("Scatter: Mean structure dose for all uncertainty scenarios");
            plots.Add("Scatter: D(V) for all uncertainty scenarios");
            plots.Add("Scatter: V(D) for all uncertainty scenarios");
            plots.Add("Scatter: OAR gradient");
            plots.Add("Scatter: Directional dose gradient as function of dose (direction)");
            plots.Add("Scatter: Directional dose gradient as function of dose (towards structure COM)");
            plots.Add("Bar: Conformity index at various dose levels - CN");
            plots.Add("Bar: Conformity index at various dose levels - CDI");
            plots.Add("Bar: Cold spot sizes");
            plots.Add("Bar: Cold spots in PlanUncertainties");
            plots.Add("Bar: Hot spot sizes");
            plots.Add("Bar: Hot spots in PlanUncertainties");
            plots.Add("Bar: Beam path lengths through ROI");
            plots.Add("Curve: Dose profile between two structures");
            plots.Add("Curve: Cumulative Gradient Index");
            plots.Add("Curve: Cumulative Gradient Index (Reff)");
            plots.Add("Curve: Directional dose gradient as function of CC position (direction)");
            plots.Add("Curve: Directional dose gradient as function of CC position (towards structure COM)");
            plots.Add("Curve: Voxelwise minimum dose - cumulative DVH");
            plots.Add("Histogram: Voxelwise dose error bar width");
            plots.Add("Histogram: Voxelwise maximum dose deviation");
            plots.Add("Histogram: Differential dose gradient index");
            plots.Add("Overlap volume histogram: Target cold volume");
            plots.Add("Overlap volume histogram: Structure hot volume");
            plots.Add("Area: Spatial DVH");

            plots.Add("Test: Dose map around OAR");


            // get the combo box reference
            var comboBox = sender as ComboBox;

            // assign the items source as the list
            comboBox.ItemsSource = plots;

            // Load last displayed plot. Default value: Empty
            var value = Properties.Settings.Default.chosenPlot;
            comboBox.SelectedIndex = plots.IndexOf(value);

        }

        private void PlotsSelectionChanged(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;

            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Clear plot
            _vm.OnClear();

            // Hide all optional elements
            HideAll();

            // Show optional elements according to plot type
            string value = comboBox.SelectedItem as string;
            switch (value)
            {
                case "Empty":
                    PlotDescription.Text = "No plot is shown. Choose a plot from the drop down menu.";
                    ExportInstructionText.Visibility = Visibility.Collapsed;
                    break;

                case "Scatter: Conformity, Homogeneity, Gradient - CN and simplified GI":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    ChooseBodyTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the conformity index [1], the homogeneity index [2], and the gradient index giving the effective distance between the chosen evaluation dose level and the isodose corresponding to 50% of this [3] for the chosen structure. Conformity index can be shown only for target structures. Black lines indicate ideal values. \n\n[1] Paddick, I (2000) A simple scoring ratio to index the conformity of radiosurgical treatment plans.\n[2] ICRU Report 83 \n[3] Adapted from e.g. Wagner, TH et al (2003) A simple and reliable index for scoring rival stereotactic radiosurgery plans";
                    break;

                case "Scatter: Conformity, Homogeneity, Gradient - CDI and GCI":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    ChooseBodyTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the conformity index [1], the homogeneity index [2], and the gradient index giving the effective distance between the chosen evaluation dose level and the isodose corresponding to 50% of this [3] for the chosen structure. Conformity index can be shown only for target structures. Black lines indicate ideal values. \n\n[1] Wu, Q-R.J. et al. (2003) Quality of coverage: Conformity measures for stereotactic radiosurgery.\n[2] ICRU Report 83 \n[3] Sung & Choi (2018) Dose gradient curve: A new tool for evaluating dose gradient.";
                    break;

                case "Scatter: OAR gradient":
                    EnterFiveDosesTitle.Visibility = Visibility.Visible;
                    FiveInputsButton.Visibility = Visibility.Visible;
                    FiveNumericInputsBox1.Visibility = Visibility.Visible;
                    FiveNumericInputsBox2.Visibility = Visibility.Visible;
                    FiveNumericInputsBox3.Visibility = Visibility.Visible;
                    FiveNumericInputsBox4.Visibility = Visibility.Visible;
                    FiveNumericInputsBox5.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the minimum distance between the chosen structure and the chosen isodose levels for the nominal plan. The distance is given in the 3D image's meshgrid spacing (ca 1mm). A value of 0 indicates that the maximum dose in the OAR lies over the chosen isodose level. \n Diamond: Absolute shortest distance \n ";
                    break;


                case "Scatter: Target cold volume size and distance from edge":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Scatter plot showing size vs distance from target edge for each separate contiguous cold volume. Shortest distances of closest and farthest points as well as 5%-95% of points (crosses).";
                    break;

                case "Scatter: Structure hot volume size and distance from structure edge":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    ChooseSecondStructureTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Scatter plot showing size vs distance from structure edge for each separate contiguous hot volume. Shortest distances of closest and farthest points as well as 5%-95% of points (crosses).";
                    break;

                case "Scatter: Mean structure dose for all uncertainty scenarios":
                    PlotDescription.Text = "Mean structure dose in all calculated uncertainty scenarios.";
                    break;

                case "Scatter: D(V) for all uncertainty scenarios":
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    AbsRelVolTitle.Visibility = Visibility.Visible;
                    AbsRelVolBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "D(V) in all calculated uncertainty scenarios.";
                    break;

                case "Scatter: V(D) for all uncertainty scenarios":
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    AbsRelDoseTitle.Visibility = Visibility.Visible;
                    AbsRelDoseBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "V(D) in all calculated uncertainty scenarios.";
                    break;

                case "Scatter: Directional dose gradient as function of dose (direction)":
                    DirectionBox.Visibility = Visibility.Visible;
                    DoseInputTitle5.Visibility = Visibility.Visible;
                    XInputTitle5.Visibility = Visibility.Visible;
                    YInputTitle5.Visibility = Visibility.Visible;
                    SpatialResInputTitle5.Visibility = Visibility.Visible;
                    DoseResInputTitle5.Visibility = Visibility.Visible;
                    FiveInputsButton.Visibility = Visibility.Visible;
                    FiveNumericInputsBox1.Visibility = Visibility.Visible;
                    FiveNumericInputsBox2.Visibility = Visibility.Visible;
                    FiveNumericInputsBox3.Visibility = Visibility.Visible;
                    FiveNumericInputsBox4.Visibility = Visibility.Visible;
                    FiveNumericInputsBox5.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the distance between the chosen reference structure and a range of isodose lines in the specified direction along a line at the specified coordinates. X and Y coordinates are given in mm relative to the field isocenter. Search resolution in space and dose is specified in the fields Sp. Res. and Dose Res.";
                    break;

                case "Scatter: Directional dose gradient as function of dose (towards structure COM)":
                    ChooseSecondStructureTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox1.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox2.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox3.Visibility = Visibility.Visible;
                    ThreeInputsButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the distance between the chosen reference structure and a range of isodose lines in the specified direction along a line towards the specified OAR. Search resolution in space and dose is specified in the fields Sp. Res. and Dose Res.";
                    break;

                case "Bar: Cold spot sizes":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Sizes of contiguous cold spots in cc.";
                    break;

                case "Bar: Hot spot sizes":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Sizes of contiguous hot spots in cc.";
                    break;

                case "Bar: Cold spots in PlanUncertainties":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Sizes of contiguous cold spots in cc in all calculated uncertainty scenarios. Scenario doses may have to be individually loaded in the UI to be visible in this plot.";
                    break;

                case "Bar: Hot spots in PlanUncertainties":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Sizes of contiguous hot spots in cc in all calculated uncertainty scenarios. Scenario doses may have to be individually loaded in the UI to be visible in this plot.";
                    break;

                case "Bar: Conformity index at various dose levels - CN":
                    EnterThreeCIDoseTitle.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox1.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox2.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox3.Visibility = Visibility.Visible;
                    ThreeInputsButton.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    ChooseBodyTitle.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the conformity index [1] with respect to the selected structure at three user-specified dose levels.\n[1] Paddick, I (2000) ) A simple scoring ratio to index the conformity of radiosurgical treatment plans.";
                    break;

                case "Bar: Conformity index at various dose levels - CDI":
                    EnterThreeCIDoseTitle.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox1.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox2.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox3.Visibility = Visibility.Visible;
                    ThreeInputsButton.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    ChooseBodyTitle.Visibility = Visibility.Visible;
                    PlotDescription.Text = "The plot shows the conformity index [1] with respect to the selected structure at three user-specified dose levels.\n[1] Wu, Q-R.J. et al. (2003) Quality of coverage: Conformity measures for stereotactic radiosurgery.";
                    break;

                case "Bar: Beam path lengths through ROI":
                    PlotDescription.Text = "The plot shows the path length (in mm) traversed by each treatment beam's central axis through the chosen structure. Only implemented for proton plans.";
                    break;

                case "Overlap volume histogram: Target cold volume":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Overlap volume histogram of the chosen structure (target) and its volume below the cutoff dose level. Enter a dose cutoff level in the box above and click button to enter.";
                    break;

                case "Overlap volume histogram: Structure hot volume":
                    EnterDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;

                    ChooseTargetTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;

                    PlotDescription.Text = "Overlap volume histogram of the structure's volume above the cutoff dose level and its distance to the chosen target edge. Enter a dose cutoff level in the box above and click button to enter.";
                    break;

                case "Curve: Dose profile between two structures":
                    ChooseSecondStructureTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Dose profile along the line connecting the two structures' centers.";
                    break;

                case "Curve: Voxelwise minimum dose - cumulative DVH":
                    PlotDescription.Text = "Voxelwise minimum dose from all calculated uncertainty scenarios.";
                    break;

                case "Curve: Cumulative Gradient Index":
                    EnterGradientDoseCutoffTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    ChooseStructureTitle.Visibility = Visibility.Collapsed;
                    StructureBox.Visibility = Visibility.Collapsed;
                    PlotDescription.Text = "Dose gradient index [1] for various dose levels with respect to the chosen reference structure. \n [1] Sung & Choi (2018) Dose gradient curve: A new tool for evaluating dose gradient";
                    break;

                case "Curve: Cumulative Gradient Index (Reff)":
                    EnterDoseTitle.Visibility = Visibility.Visible;
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    ChooseBodyTitle.Visibility = Visibility.Visible;
                    ChooseStructureTitle.Visibility = Visibility.Collapsed;
                    PlotDescription.Text = "Dose gradient index [1] for various dose levels with respect to the chosen reference dose.\n [1] GI = Reff(evaluation isodose) - Reff(reference isodose)  ";
                    break;

                case "Curve: Directional dose gradient as function of CC position (direction)":
                    DirectionBox.Visibility = Visibility.Visible;
                    DoseInputTitle5.Visibility = Visibility.Visible;
                    XInputTitle5.Visibility = Visibility.Visible;
                    YInputTitle5.Visibility = Visibility.Visible;
                    SpatialResInputTitle5.Visibility = Visibility.Visible;
                    DoseResInputTitle5.Visibility = Visibility.Visible;
                    FiveInputsButton.Visibility = Visibility.Visible;
                    FiveNumericInputsBox1.Visibility = Visibility.Visible;
                    FiveNumericInputsBox2.Visibility = Visibility.Visible;
                    FiveNumericInputsBox3.Visibility = Visibility.Visible;
                    FiveNumericInputsBox4.Visibility = Visibility.Visible;
                    FiveNumericInputsBox5.Visibility = Visibility.Visible;

                    PlotDescription.Text = "Distance in the chosen anatomical directions to chosen isodose level for each CC slice on which the target is delineated. Set vertical/horizontal location of the search line in the second input box. Position is given in mm relative to the isocenter.";
                    break;

                case "Curve: Directional dose gradient as function of CC position (towards structure COM)":
                    ChooseSecondStructureTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    ThreeInputsDoseTitle.Visibility = Visibility.Visible;
                    ThreeInputsSpResTitle.Visibility = Visibility.Visible;
                    ThreeInputsDoseResTitle.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox1.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox2.Visibility = Visibility.Visible;
                    ThreeNumericInputsBox3.Visibility = Visibility.Visible;
                    ThreeInputsButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "";
                    break;

                case "Histogram: Voxelwise dose error bar width":
                    PlotDescription.Text = "Histogram showing the number of voxels in which the dose varies a given amount over all calculated uncertainty scenarios.";
                    break;

                case "Histogram: Voxelwise maximum dose deviation":
                    PlotDescription.Text = "Histogram showing the number of voxels with a certain maximum dose deviation over all uncertainty scenarios.";
                    break;

                case "Histogram: Differential dose gradient index":
                    ChooseStructureTitle.Visibility = Visibility.Collapsed;
                    StructureBox.Visibility = Visibility.Collapsed;
                    PlotDescription.Text = "Differential dose gradient index [1]. The dose gradient index describes the average distance between adjacent isodoses.";
                    break;

                case "Area: Spatial DVH":
                    DefineBorderTitle.Visibility = Visibility.Visible;
                    TwoNumericInputsBox1.Visibility = Visibility.Visible;
                    TwoNumericInputsBox2.Visibility = Visibility.Visible;
                    ChooseSecondStructureTitle.Visibility = Visibility.Visible;
                    SecondStructureBox.Visibility = Visibility.Visible;
                    TwoInputsButton.Visibility = Visibility.Visible;
                    PlotDescription.Text = "Spatial DVH for the chosen structure and subvolume borders.";
                    break;

                case "Test: Dose map around OAR":
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    break;
            }

            // Set plot type property in the view model
            _vm.ChosenPlotType = value;

            // Show plot
            _vm.ShowPlot();

            // Save selected value 
            Properties.Settings.Default.chosenPlot = value;
        }
        #endregion

        #region Structure selection logic
        // Set comboBox selection at startup
        private void FindSelectedStructure(object comboBoxObject, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)comboBoxObject;
            // load last selected structure. Default: BODY or first structure in the set
            var value = Properties.Settings.Default.chosenStructure;
            try
            {
                comboBox.SelectedItem = _vm.Structures.FirstOrDefault(s => s.Id == value);
            }
            catch
            {
                comboBox.SelectedIndex = 0;
            }
        }

        // Upon change of selection
        private void StructureSelected(object comboBoxObject, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all series from plot
            _vm.RemoveAllSeries();

            var structure = GetStructure(comboBoxObject);
            string Id = structure.Id;
            _vm.ChosenStructureId = Id;

            // Save selected value to properties
            Properties.Settings.Default.chosenStructure = Id;
        }

        private Structure GetStructure(object comboBoxObject)
        {
            var comboBox = (ComboBox)comboBoxObject;
            var structure = (Structure)comboBox.SelectedItem;
            return structure;
        }
        #endregion

        #region Plan selection logic
        private void Plan_OnChecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.AddPlanItem(GetPlan(checkBoxObject));
        }

        private void Plan_OnUnchecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.RemovePlanItem(GetPlan(checkBoxObject));
        }

        private PlanSetup GetPlan(object checkBoxObject)
        {
            var checkbox = (CheckBox)checkBoxObject;
            var planName = (PlanName)checkbox.DataContext;
            var plan = _vm.PlansInScope.FirstOrDefault(p => p.Id == planName.Name);
            return plan;
        }
        #endregion

        #region Optional input logic
        // Hide all optional fields
        private void HideAll()
        {
            ExportInstructionText.Visibility = Visibility.Visible; // this is collapsed when "empty" is chosen. 

            ChooseStructureTitle.Visibility = Visibility.Visible;
            StructureBox.Visibility = Visibility.Visible;

            NumericInputBox.Visibility = Visibility.Collapsed;
            InputButton.Visibility = Visibility.Collapsed;

            TwoNumericInputsBox1.Visibility = Visibility.Collapsed;
            TwoNumericInputsBox2.Visibility = Visibility.Collapsed;
            TwoInputsButton.Visibility = Visibility.Collapsed;

            ThreeNumericInputsBox1.Visibility = Visibility.Collapsed;
            ThreeNumericInputsBox2.Visibility = Visibility.Collapsed;
            ThreeNumericInputsBox3.Visibility = Visibility.Collapsed;
            ThreeInputsButton.Visibility = Visibility.Collapsed;
            ThreeInputsDoseTitle.Visibility = Visibility.Collapsed;
            ThreeInputsSpResTitle.Visibility = Visibility.Collapsed;
            ThreeInputsDoseResTitle.Visibility = Visibility.Collapsed;

            FiveNumericInputsBox1.Visibility = Visibility.Collapsed;
            FiveNumericInputsBox2.Visibility = Visibility.Collapsed;
            FiveNumericInputsBox3.Visibility = Visibility.Collapsed;
            FiveNumericInputsBox4.Visibility = Visibility.Collapsed;
            FiveNumericInputsBox5.Visibility = Visibility.Collapsed;
            FiveInputsButton.Visibility = Visibility.Collapsed;
            DoseInputTitle5.Visibility = Visibility.Collapsed;
            XInputTitle5.Visibility = Visibility.Collapsed;
            YInputTitle5.Visibility = Visibility.Collapsed;
            SpatialResInputTitle5.Visibility = Visibility.Collapsed;
            DoseResInputTitle5.Visibility = Visibility.Collapsed;

            EnterDoseCutoffTitle.Visibility = Visibility.Collapsed;
            EnterDoseTitle.Visibility = Visibility.Collapsed;
            EnterThreeCIDoseTitle.Visibility = Visibility.Collapsed;
            EnterDistanceTitle.Visibility = Visibility.Collapsed;
            DefineBorderTitle.Visibility = Visibility.Collapsed;
            ChooseSecondStructureTitle.Visibility = Visibility.Collapsed;
            ChooseBodyTitle.Visibility = Visibility.Collapsed;
            ChooseTargetTitle.Visibility = Visibility.Collapsed;
            SecondStructureBox.Visibility = Visibility.Collapsed;
            EnterGradientDoseCutoffTitle.Visibility = Visibility.Collapsed;

            AbsRelDoseTitle.Visibility = Visibility.Collapsed;
            AbsRelVolTitle.Visibility = Visibility.Collapsed;
            AbsRelDoseBox.Visibility = Visibility.Collapsed;
            AbsRelVolBox.Visibility = Visibility.Collapsed;

            DirectionBox.Visibility = Visibility.Collapsed;
        }

        private void InputButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry = NumericInputBox.Text;
            if (IsDecimal(entry))
            {
                _vm.NumericInput_1 = Convert.ToDouble(entry);
                MessageBox.Show(string.Format("Input value set to {0}", entry));

                // Save current value to properties
                Properties.Settings.Default.numericInput_1 = Convert.ToDouble(entry);
            }
            else
                MessageBox.Show("Input must be a decimal value!");
        }

        private void TwoInputsButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry1 = TwoNumericInputsBox1.Text;
            var entry2 = TwoNumericInputsBox2.Text;

            if (IsDecimal(entry1) & IsDecimal(entry2))
            {
                _vm.NumericInput_1 = Convert.ToDouble(entry1);
                _vm.NumericInput_2 = Convert.ToDouble(entry2);

                // save values to properties
                _vm.NumericInput_1 = Convert.ToDouble(entry1);
                _vm.NumericInput_2 = Convert.ToDouble(entry2);
            }
            else
                MessageBox.Show("Input must be a decimal value!");
        }

        private void ThreeInputsButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry1 = ThreeNumericInputsBox1.Text;
            var entry2 = ThreeNumericInputsBox2.Text;
            var entry3 = ThreeNumericInputsBox3.Text;

            if (IsDecimal(entry1) & IsDecimal(entry2))
            {
                _vm.NumericInput_1 = Convert.ToDouble(entry1);
                _vm.NumericInput_2 = Convert.ToDouble(entry2);
                _vm.NumericInput_3 = Convert.ToDouble(entry3);

                MessageBox.Show(string.Format("Inputs set to {0}, {1}, and {2}.", entry1, entry2, entry3));

                // Save values to properties
                Properties.Settings.Default.numericInput_1 = Convert.ToDouble(entry1);
                Properties.Settings.Default.numericInput_2 = Convert.ToDouble(entry2);
                Properties.Settings.Default.numericInput_3 = Convert.ToDouble(entry3);

                // set xaxis to show new input values
                if (_vm.ChosenPlotType == "Barplot: Conformity index at various dose levels")
                {
                    OxyPlot.Axes.CategoryAxis xaxis = (OxyPlot.Axes.CategoryAxis)_vm.PlotModel.Axes.First(ax => ax.Position == OxyPlot.Axes.AxisPosition.Bottom);
                    xaxis.Labels.Clear();
                    xaxis.Labels.AddRange(new List<string> { entry1 + " Gy", entry2 + " Gy", entry3 + " Gy" });
                    _vm.PlotModel.InvalidatePlot(true);
                }
            }
            else
                MessageBox.Show("Input must be a decimal value!");
        }

        private void FiveInputsButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry1 = FiveNumericInputsBox1.Text;
            var entry2 = FiveNumericInputsBox2.Text;
            var entry3 = FiveNumericInputsBox3.Text;
            var entry4 = FiveNumericInputsBox4.Text;
            var entry5 = FiveNumericInputsBox5.Text;

            if (IsDecimal(entry1) & IsDecimal(entry2) & IsDecimal(entry3) & IsDecimal(entry4) & IsDecimal(entry5))
            {
                _vm.NumericInput_1 = Convert.ToDouble(entry1);
                _vm.NumericInput_2 = Convert.ToDouble(entry2);
                _vm.NumericInput_3 = Convert.ToDouble(entry3);
                _vm.NumericInput_4 = Convert.ToDouble(entry4);
                _vm.NumericInput_5 = Convert.ToDouble(entry5);

                MessageBox.Show(string.Format("Input values set to {0}, {1}, {2}, {3}, and {4}.", entry1, entry2, entry3, entry4, entry5));

                // Save values
                Properties.Settings.Default.numericInput_1 = Convert.ToDouble(entry1);
                Properties.Settings.Default.numericInput_2 = Convert.ToDouble(entry2);
                Properties.Settings.Default.numericInput_3 = Convert.ToDouble(entry3);
                Properties.Settings.Default.numericInput_4 = Convert.ToDouble(entry4);
                Properties.Settings.Default.numericInput_5 = Convert.ToDouble(entry5);
            }
            else
            {
                MessageBox.Show("Inputs must all be decimal values!");
            }
        }

        private void FindSecondSelectedStructure(object comboBoxObject, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)comboBoxObject;
            // read input from file, if it exists
            try
            {
                var value = Properties.Settings.Default.chosenSecondStructure;
                comboBox.SelectedItem = _vm.Structures.FirstOrDefault(s => s.Id == value);
            }
            catch
            {
                comboBox.SelectedItem = _vm.Structures.First();
            }
        }

        private void SecondStructureSelected(object comboBoxObject, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            _vm.RemoveAllSeries();
            var structure = GetStructure(comboBoxObject);
            _vm.ChosenSecondStructureId = structure.Id;

            // Save selected value to file
            Properties.Settings.Default.chosenSecondStructure = structure.Id;
        }

        private void AbsRelDoseLoaded(object sender, RoutedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;

            box.Items.Add("Absolute [Gy]");
            box.Items.Add("Relative [%]");

            box.SelectedIndex = 0;
        }

        private void AbsRelVolLoaded(object sender, RoutedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;

            box.Items.Add("Absolute [cc]");
            box.Items.Add("Relative [%]");

            box.SelectedIndex = 0;
        }

        private void AbsRelDoseChanged(object sender, RoutedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            _vm.AbsRelDose = (string)box.SelectedItem;
            _vm.UncheckAllBoxes();
            _vm.RemoveAllSeries();
        }

        private void AbsRelVolChanged(object sender, RoutedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            _vm.AbsRelVol = (string)box.SelectedItem;
            _vm.UncheckAllBoxes();
            _vm.RemoveAllSeries();
        }

        private void DirectionLoaded(object sender, RoutedEventArgs e) // at some point change this to check boxes
        {
            // List of possible directions to choose from.
            List<string> dir = new List<string>();

            dir.Add("AP");
            dir.Add("LR");

            // get the combo box reference
            var comboBox = sender as ComboBox;

            // assign the items source as the list
            comboBox.ItemsSource = dir;

            // Load last displayed plot. Default value: AP
            var value = Properties.Settings.Default.chosenDirection;
            comboBox.SelectedIndex = dir.IndexOf(value);
        }

        private void DirectionChanged(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            _vm.UncheckAllBoxes();

            // remove all series from plotmodel
            _vm.RemoveAllSeries();

            ComboBox cb = sender as ComboBox;

            _vm.Direction = (string)cb.SelectedItem;
        }

        private bool IsDecimal(string input)
        {
            Decimal dummy;
            return Decimal.TryParse(input, out dummy);
        }
        #endregion

        #region Export logic
        private void ExportPlotAsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.ExportPlotAsPdf(filePath);
        }

        private void ExportDataAsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.ExportDataAsTxt(filePath);
        }

        private void ExportDataAsCsv(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.ExportDataAsTxt(filePath);
        }

        private string GetPdfSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export to PDF",
                Filter = "PDF Files (*.pdf)|*.pdf"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetTxtSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to txt file",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetCsvSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to csv file",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
        #endregion
    }
}
