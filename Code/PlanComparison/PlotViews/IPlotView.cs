﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;

namespace PlanComparison.PlotViews
{
    public interface IPlotView
    {
        // Methods
        void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels);
        void SetTitle(PlotModel plotModel, string title);
        void SetLegend(PlotModel plotModel, bool isLegendVisible, OxyPlot.LegendPlacement placement, OxyPlot.LegendPosition position, OxyPlot.LegendOrientation orientation);
    }
}
