﻿#pragma checksum "..\..\MainView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "F0315A3BA0C1945A0C3926C0108C8122EEEE579172EF2AF2BB6C9D152AE7AB99"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using OxyPlot.Wpf;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace PlanComparison {
    
    
    /// <summary>
    /// MainView
    /// </summary>
    public partial class MainView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 21 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ChooseStructureTitle;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox StructureBox;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ChooseSecondStructureTitle;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ChooseTargetTitle;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ChooseBodyTitle;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterGradientDoseCutoffTitle;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox SecondStructureBox;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterDoseTitle;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterDoseCutoffTitle;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterDistanceTitle;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DefineBorderTitle;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterThreeCIDoseTitle;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock EnterFiveDosesTitle;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ThreeInputsDoseTitle;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ThreeInputsSpResTitle;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ThreeInputsDoseResTitle;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox NumericInputBox;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ThreeNumericInputsBox1;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ThreeNumericInputsBox2;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ThreeNumericInputsBox3;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TwoNumericInputsBox1;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TwoNumericInputsBox2;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DoseInputTitle5;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FiveNumericInputsBox1;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock XInputTitle5;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FiveNumericInputsBox2;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock YInputTitle5;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FiveNumericInputsBox3;
        
        #line default
        #line hidden
        
        
        #line 149 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SpatialResInputTitle5;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FiveNumericInputsBox4;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DoseResInputTitle5;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FiveNumericInputsBox5;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button InputButton;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button TwoInputsButton;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ThreeInputsButton;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button FiveInputsButton;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock AbsRelDoseTitle;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock AbsRelVolTitle;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AbsRelDoseBox;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AbsRelVolBox;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox DirectionBox;
        
        #line default
        #line hidden
        
        
        #line 182 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PlotDescription;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\MainView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock ExportInstructionText;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PlanComparisonV15.esapi;component/mainview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 17 "..\..\MainView.xaml"
            ((System.Windows.Controls.ComboBox)(target)).Loaded += new System.Windows.RoutedEventHandler(this.PlotsComboBoxLoaded);
            
            #line default
            #line hidden
            
            #line 18 "..\..\MainView.xaml"
            ((System.Windows.Controls.ComboBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.PlotsSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ChooseStructureTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.StructureBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 27 "..\..\MainView.xaml"
            this.StructureBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.StructureSelected);
            
            #line default
            #line hidden
            
            #line 28 "..\..\MainView.xaml"
            this.StructureBox.Loaded += new System.Windows.RoutedEventHandler(this.FindSelectedStructure);
            
            #line default
            #line hidden
            return;
            case 4:
            this.ChooseSecondStructureTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.ChooseTargetTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.ChooseBodyTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.EnterGradientDoseCutoffTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.SecondStructureBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 46 "..\..\MainView.xaml"
            this.SecondStructureBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SecondStructureSelected);
            
            #line default
            #line hidden
            
            #line 47 "..\..\MainView.xaml"
            this.SecondStructureBox.Loaded += new System.Windows.RoutedEventHandler(this.FindSecondSelectedStructure);
            
            #line default
            #line hidden
            return;
            case 10:
            this.EnterDoseTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.EnterDoseCutoffTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.EnterDistanceTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.DefineBorderTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.EnterThreeCIDoseTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.EnterFiveDosesTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.ThreeInputsDoseTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.ThreeInputsSpResTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.ThreeInputsDoseResTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.NumericInputBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.ThreeNumericInputsBox1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 21:
            this.ThreeNumericInputsBox2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 22:
            this.ThreeNumericInputsBox3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 23:
            this.TwoNumericInputsBox1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.TwoNumericInputsBox2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.DoseInputTitle5 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 26:
            this.FiveNumericInputsBox1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.XInputTitle5 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.FiveNumericInputsBox2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.YInputTitle5 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 30:
            this.FiveNumericInputsBox3 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.SpatialResInputTitle5 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 32:
            this.FiveNumericInputsBox4 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 33:
            this.DoseResInputTitle5 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 34:
            this.FiveNumericInputsBox5 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 35:
            this.InputButton = ((System.Windows.Controls.Button)(target));
            
            #line 158 "..\..\MainView.xaml"
            this.InputButton.Click += new System.Windows.RoutedEventHandler(this.InputButtonClick);
            
            #line default
            #line hidden
            return;
            case 36:
            this.TwoInputsButton = ((System.Windows.Controls.Button)(target));
            
            #line 159 "..\..\MainView.xaml"
            this.TwoInputsButton.Click += new System.Windows.RoutedEventHandler(this.TwoInputsButtonClick);
            
            #line default
            #line hidden
            return;
            case 37:
            this.ThreeInputsButton = ((System.Windows.Controls.Button)(target));
            
            #line 160 "..\..\MainView.xaml"
            this.ThreeInputsButton.Click += new System.Windows.RoutedEventHandler(this.ThreeInputsButtonClick);
            
            #line default
            #line hidden
            return;
            case 38:
            this.FiveInputsButton = ((System.Windows.Controls.Button)(target));
            
            #line 161 "..\..\MainView.xaml"
            this.FiveInputsButton.Click += new System.Windows.RoutedEventHandler(this.FiveInputsButtonClick);
            
            #line default
            #line hidden
            return;
            case 39:
            this.AbsRelDoseTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 40:
            this.AbsRelVolTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.AbsRelDoseBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 176 "..\..\MainView.xaml"
            this.AbsRelDoseBox.Loaded += new System.Windows.RoutedEventHandler(this.AbsRelDoseLoaded);
            
            #line default
            #line hidden
            
            #line 176 "..\..\MainView.xaml"
            this.AbsRelDoseBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AbsRelDoseChanged);
            
            #line default
            #line hidden
            return;
            case 42:
            this.AbsRelVolBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 177 "..\..\MainView.xaml"
            this.AbsRelVolBox.Loaded += new System.Windows.RoutedEventHandler(this.AbsRelVolLoaded);
            
            #line default
            #line hidden
            
            #line 177 "..\..\MainView.xaml"
            this.AbsRelVolBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AbsRelVolChanged);
            
            #line default
            #line hidden
            return;
            case 43:
            this.DirectionBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 180 "..\..\MainView.xaml"
            this.DirectionBox.Loaded += new System.Windows.RoutedEventHandler(this.DirectionLoaded);
            
            #line default
            #line hidden
            
            #line 180 "..\..\MainView.xaml"
            this.DirectionBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.DirectionChanged);
            
            #line default
            #line hidden
            return;
            case 44:
            this.PlotDescription = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 45:
            this.ExportInstructionText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 46:
            
            #line 192 "..\..\MainView.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.ExportPlotAsPdf);
            
            #line default
            #line hidden
            return;
            case 47:
            
            #line 196 "..\..\MainView.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.ExportDataAsTxt);
            
            #line default
            #line hidden
            return;
            case 48:
            
            #line 200 "..\..\MainView.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.ExportDataAsCsv);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 9:
            
            #line 67 "..\..\MainView.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Checked += new System.Windows.RoutedEventHandler(this.Plan_OnChecked);
            
            #line default
            #line hidden
            
            #line 68 "..\..\MainView.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.Plan_OnUnchecked);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

