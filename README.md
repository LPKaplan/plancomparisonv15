# PlanComparisonV15

This binary plugin contains multiple different plan comparison metrics and visualizations. It is being developed continuously and is NOT officially approved for clinical use.
Metric calculations are implemented in the project MetricsV15 (separate repository). A compiled dll of the newest version is included in this project.
