﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;

namespace PlanComparison.PlotViews
{
    class BarPlotMultipleXAxes
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string[] xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            // Axes
            // x-axis
            var xAxis = new CategoryAxis
            {
                Title = xLabel[0],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                MajorGridlineStyle = LineStyle.Solid,
                AxisTitleDistance = 15,
                Position = AxisPosition.Bottom,
                Angle = 90,
                Key = xLabel[0]
            };

            xAxis.Labels.AddRange(xTickLabels);

            plotModel.Axes.Add(xAxis);

            // y-axis
            var yAxis = new LinearAxis
            {
                Title = yLabel[0],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = yLabel[0]
            };

            plotModel.Axes.Add(yAxis);
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, OxyPlot.LegendPlacement placement, OxyPlot.LegendPosition position, OxyPlot.LegendOrientation orientation)
        {
            //legend
            plotModel.LegendPlacement = placement;
            plotModel.LegendBorder = OxyColors.Black;
            plotModel.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            plotModel.LegendPosition = position;
            plotModel.LegendOrientation = orientation;
            plotModel.IsLegendVisible = isLegendVisible;
        }
        public void SetTitle(PlotModel plotModel, string title)
        {
            // Title
            plotModel.Title = title;
        }
    }
}
